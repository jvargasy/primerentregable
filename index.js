function guardarEnSessionStorage() {
    var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
    var txtValor = document.getElementById("txtValor"); /* Referencia al input de valor */
    var clave = txtClave.value;
    var valor = txtValor.value;
    sessionStorage.setItem(clave, valor);
    var objeto = {
      nombre:"Ezequiel",
      apellidos:"Llarena Borges",
      ciudad:"Madrid",
      pais:"España"
    };
    sessionStorage.setItem("json", JSON.stringify(objeto));
  }
  function leerDeSessionStorage() {
    var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
    var clave = txtClave.value;
    var valor = sessionStorage.getItem(clave);
    var spanValor = document.getElementById("spanValor");
    spanValor.innerText = valor;
    var datosUsuario = JSON.parse(sessionStorage.getItem("json"));
    console.log(datosUsuario.nombre);
    console.log(datosUsuario.pais);
    console.log(datosUsuario);
  }

  function EliminarEnSessionStorage() {
    var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
    var clave = txtClave.value;
    var valor = sessionStorage.removeItem(clave)
    spanValor.innerText = "Session eliminado : " + clave;
  }

  function ClearEnSessionStorage() {
    sessionStorage.clear();
    var spanValor = document.getElementById("spanValor");
    spanValor.innerText = "Se limpio todos los datos de sessionStorage ";
  }

  function lenghEnSessionStorage() {
    var numElementSaved = sessionStorage.length;
    var spanValor = document.getElementById("spanValor");
    spanValor.innerText = "Número de elementos guardados en sessión: " + numElementSaved;

  }
